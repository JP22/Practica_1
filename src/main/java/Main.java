import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.Scanner;


public class Main {
    public static void main (String[] args){
        try{
            boolean fPost = true;
            int parAmount = 0,imgAmount=0,postForms=0,getForms=0,j=1;
            String URL = "";
            Scanner s = new Scanner(System.in);
            URL = s.nextLine();


            try{

                Connection conn = Jsoup.connect(URL);
                Document doc = conn.get();


                String [] aux = doc.baseUri().split("/");
                String base = aux[0]+"//"+aux[2];
                int lines = conn.execute().body().split("\n").length;
                for(Element it: doc.select("p")){
                    parAmount++;
                    for(Element i: it.select("img")){
                        imgAmount++;
                    }
                }
                for(Element it: doc.select("form")){
                    if (it.attr("method").toLowerCase().equals("post")) {
                        postForms++;
                    }
                    else if (it.attr("method").toLowerCase().equals("get")) getForms++;
                }
                System.out.println("Cantidad de lineas: "+lines);
                System.out.println("Cantidad de parrafos: "+parAmount);
                System.out.println("Cantidad de imagenes dentro de parrafos: "+imgAmount);
                System.out.println("Cantidad de formularios: "+(postForms+getForms));
                System.out.println("Cantidad de formularios POST: "+postForms);
                System.out.println("Cantidad de formularios GET: "+getForms);
                for (Element it: doc.select("form")){
                    String method = it.attr("method").toLowerCase();
                    if (method.equals("post") || method.equals("get")){
                        System.out.println("Formulario "+j+" ("+method.toUpperCase()+")"+":");
                        int count = 1;
                        for (Element i: it.select("input")) {
                            System.out.print("Nombre del input "+count+": "+i.attr("name"));
                            System.out.println(" --> Tipo del input "+count+": "+i.attr("type"));
                            count++;
                        }
                        j++;
                    }
                    if (method.equals("post") && fPost){
                        try{
                            Document documento = Jsoup.connect(base+it.attr("action"))
                                    .data("asignatura", "practica1")
                                    .header("matricula","20132045")
                                    .post();
                            System.out.println(documento.body().html());
                        }
                        catch (Exception e){
                            fPost = false;
                            System.out.println("Este servidor no cuenta con funcion POST");
                        }
                    }
                }


            }
            catch (Exception e){
                System.out.println("El URL es invalido");
            }
        }
        catch(Exception e){
            e.printStackTrace();

        }
    }
}
